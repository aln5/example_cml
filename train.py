from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import ConfusionMatrixDisplay
import matplotlib.pyplot as plt
import json
import os
import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import load_diabetes
from sklearn.linear_model import Ridge
from sklearn.metrics import PredictionErrorDisplay
from sklearn.metrics import classification_report


# Read in data
X_train = np.genfromtxt("data/train_features.csv")
y_train = np.genfromtxt("data/train_labels.csv")
X_test = np.genfromtxt("data/test_features.csv")
y_test = np.genfromtxt("data/test_labels.csv")

# Fit a model
depth = 5
clf = RandomForestClassifier(max_depth=depth)
clf.fit(X_train, y_train)

acc = clf.score(X_test, y_test)
print(acc)
with open("metrics.txt", "w") as outfile:
    outfile.write("Accuracy: " + str(acc) + "\n")

# Plot it
disp = ConfusionMatrixDisplay.from_estimator(
    clf, X_test, y_test, normalize="true", cmap=plt.cm.Blues
)
plt.savefig("plot.png")



X, y = load_diabetes(return_X_y=True)
ridge = Ridge().fit(X, y)
y_pred = ridge.predict(X)
display = PredictionErrorDisplay(y_true=y, y_pred=y_pred)
display.plot()
plt.savefig("plot2.png")

##

def test_dataset():
    X_test = np.genfromtxt("data/test_features.csv")
    y_test = np.genfromtxt("data/test_labels.csv")
    return X_test, y_test

# fetch model
def model():
    X_train = np.genfromtxt("data/train_features.csv")
    y_train = np.genfromtxt("data/train_labels.csv")
    # Fit a model
    depth = 5
    clf = RandomForestClassifier(max_depth=depth)
    clf.fit(X_train, y_train)
    return clf
def metrics(model, test_dataset):
    metrics = classification_report(y_true=test_dataset[1],
                                    y_pred=model.predict(test_dataset[0]),
                                    output_dict=True)

    print(metrics)
    return metrics


print(metrics(model=model(),test_dataset=test_dataset()))