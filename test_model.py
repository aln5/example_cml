import pytest
from sklearn.datasets import load_wine
from sklearn.ensemble import RandomForestClassifier
import numpy as np
from sklearn.metrics import classification_report
import warnings

# fetch data
@pytest.fixture
def test_dataset():
    X_test = np.genfromtxt("data/test_features.csv")
    y_test = np.genfromtxt("data/test_labels.csv")
    return X_test, y_test

# fetch model
@pytest.fixture
def model():
    X_train = np.genfromtxt("data/train_features.csv")
    y_train = np.genfromtxt("data/train_labels.csv")
    # Fit a model
    depth = 5
    clf = RandomForestClassifier(max_depth=depth)
    clf.fit(X_train, y_train)
    return clf
@pytest.fixture
def metrics(model, test_dataset):
    metrics = classification_report(y_true=test_dataset[1],
                                    y_pred=model.predict(test_dataset[0]),
                                    output_dict=True)

    print(metrics)
    return metrics
def test_model_f1(metrics):
    value = metrics['0.0']['f1-score']
    if value < 0.9:
        warnings.warn(f'Performance Warning: f1 is {value} which is below expectations')
    assert value > 0.5

def test_model_precision(metrics):
    value = metrics['0.0']['precision']
    if value < 0.9:
        warnings.warn(f'Performance Warning: precision is {value} which is below expectations')
    assert value > 0.5

def test_model_recall(metrics):
    value = metrics['0.0']['recall']
    if value < 0.9:
        warnings.warn(f'Performance Warning: recall is {value} which is below expectations')
    assert value > 0.5